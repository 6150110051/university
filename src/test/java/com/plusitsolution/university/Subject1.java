package com.plusitsolution.university;

public class Subject1 {
	private String subjectID;
	private String subjectName;
	private Integer credit;
	
	
	public Subject1(String subjectID, String subjectName, Integer credit) {
		super();
		this.subjectID = subjectID;
		this.subjectName = subjectName;
		this.credit = credit;
	}
	public String getSubjectID() {
		return subjectID;
	}
	public void setSubjectID(String subjectID) {
		this.subjectID = subjectID;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public Integer getCredit() {
		return credit;
	}
	public void setCredit(Integer credit) {
		this.credit = credit;
	}
	@Override
	public String toString() {
		return "Subject1 [subjectID = " + subjectID + ", subjectName = " + subjectName + ", credit = " + credit + "]";
	}

}
