package com.plusitsolution.university;



public class Student1 {
	private String studentID;
	private String fristName, lastName;
	private Branch branch;



	public Student1(String studentID, String fristName, String lastName, Branch branch) {
		super();
		this.studentID = studentID;
		this.fristName = fristName;
		this.lastName = lastName;
		this.branch = branch;
	}

	public String getStudentID() {
		return studentID;
	}
	public void setStudentID(String studentID) {
		this.studentID = studentID;
	}
	public String getFristName() {
		return fristName;
	}
	public void setFristName(String fristName) {
		this.fristName = fristName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Branch getBranch() {
		return branch;
	}
	public void setBranch(Branch branch) {
		this.branch = branch;
	}


	@Override
	public String toString() {
		return "Student [studentID = " + studentID + ", fristName = " + fristName + ", lastName=" + lastName + ", branch = "
				+ branch + "]";
	}
}
