package com.plusitsolution.university;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicLong;


public class University1 {
	public static void main(String[] args) {
		registerStudent("Pond", "Keawisad", Branch.Account);
		registerStudent("Pee", "Sangthong", Branch.Marketing);
		registerStudent("Le", "Abc", Branch.Marketing);
		registerStudent("Le", "Abc", Branch.Computer);
		registerStudent("Le", "Abc", Branch.Computer);

		listStudent();

		addSubject("Maths", 10);
		addSubject("Finance", 8);
		addSubject("Science", 5);
		addSubject("Society", 10);
		listSubject();

		apply("61101", "S2");
		apply("61101", "S3");
		apply("61101", "S4");
//		apply("61101", "S1");
//		apply("61101", "S1");

		describe("61101");
	}

	private static Map<String, Student1> STUDENT_MAP = new TreeMap<String, Student1>();

	private static Map<String, Subject1> SUBJECT_MAP = new HashMap<String, Subject1>();
	
	private static Map<String, ArrayList<String>> REGISTERSTUDY_MAP1 = new HashMap<String, ArrayList<String>>();
	
	private static AtomicLong counter = new AtomicLong();

	public static String registerStudent(String fristName, String lastName, Branch branch) {

		long sId = 610 + counter.incrementAndGet();
		String studentID = "";

		// ถ้าสาขา ACC จะให้เลข 01 หลังรหัสที่เข้า
		if (branch == Branch.Account) {
			studentID = Long.toString(sId) + "01";
			Student1 student = new Student1(studentID, fristName, lastName, branch);
			STUDENT_MAP.put(studentID, student);
			

			// ถ้าสาขา MKT จะให้เลข 02 หลังรหัสที่เข้า
		} else if (branch == Branch.Marketing) {
			studentID = Long.toString(sId) + "02";
			Student1 student = new Student1(studentID, fristName, lastName, branch);
			STUDENT_MAP.put(studentID, student);

			// ถ้าสาขา COM จะให้เลข 03 หลังรหัสที่เข้า
		} else if (branch == Branch.Computer) {
			studentID = Long.toString(sId) + "03";
			Student1 student = new Student1(studentID, fristName, lastName, branch);
			STUDENT_MAP.put(studentID, student);
		}
		
		return studentID;

	}

	public static void listStudent() {
		System.out.println("------ All list student ------");
		for (Entry<String, Student1> entry : STUDENT_MAP.entrySet()) {
			System.out.println(entry.getValue());
		}
		System.out.println("------------------------------");
	}

	public static String addSubject(String subjectName, Integer credit) {
		String subjectID = "S" + (SUBJECT_MAP.size() + 1);
		Subject1 subjec = new Subject1(subjectID, subjectName, credit);
		SUBJECT_MAP.put(subjectID, subjec);
		return subjectID;
	}

	public static void listSubject() {
		System.out.println("------ All list subject ------");
		for (Entry<String, Subject1> entry : SUBJECT_MAP.entrySet()) {
			System.out.println(entry.getValue());
		}
		System.out.println("------------------------------");
	}

	public static void apply(String studentID, String subjectID) {

		ArrayList<String> rgtList = REGISTERSTUDY_MAP1.get(studentID);
		int total = 0;
		
		if (STUDENT_MAP.containsKey(studentID)) {
			if (rgtList == null) {
				rgtList = new ArrayList<String>();
				rgtList.add(SUBJECT_MAP.get(subjectID).getSubjectID());
			} else {
				// ค้นหาค่าในสตริง
				if (-1 == rgtList.indexOf(subjectID)) {
					
//					!rgtList.contains(subjectID)
					rgtList.add(SUBJECT_MAP.get(subjectID).getSubjectID());

					for (String subJ : rgtList) {
						total += SUBJECT_MAP.get(subJ).getCredit();
					}
					if (total > 24) {
						rgtList.remove(subjectID);
						throw new IllegalArgumentException("Cannot apply existing subject");
					}
				} else {
					throw new IllegalArgumentException("Repeat course code ");
				}
			}
			REGISTERSTUDY_MAP1.put(studentID, rgtList);
		} else {
			throw new IllegalArgumentException("Wrong student ID ");
		}
	}

	public static void describe(String studentID) {
		System.out.println("------ Describe ------");
		ArrayList<String> rgtList = REGISTERSTUDY_MAP1.get(studentID);

		Student1 student = STUDENT_MAP.get(studentID);
		System.out.println(student.getStudentID() + " " + student.getFristName() + " " + student.getLastName());
		int total = 0;
		for (String subJ : rgtList) {

			total += SUBJECT_MAP.get(subJ).getCredit();
			System.out.println(SUBJECT_MAP.get(subJ).getSubjectID() + " " + SUBJECT_MAP.get(subJ).getSubjectName());
		}
		System.out.println("TotalCredit = " + total);

	}
}
